@extends('layouts.style')

@section('content')
    <script>
        $.ajax({
            url: '/all-color',
            type: 'GET',
            success: function (data) {
                $.each(data, function (index, element) {
                    $('#color'+element.color_id).css('background', element.hex)
                })
            }
        })
    </script>
    <div class="col-sm-12 center">
        <h1 class="montserrat">SURF YOUR COLOR INSPIRATION</h1>
        <h5 class="raleway mt10">You can search color or palettes that you want or just looking for inspiration here.</h5>
    </div>
    <div class="col-sm-12 mt50">
        <div class="row">
            @foreach($color as $item)
                <div class="col-sm-2">
                    <div class="thumbnail">
                        <a href=""><div class="box-color" id="color{{ $item->color_id }}"></div></a>
                        <p class="center montserrat mt10"><a href="">{{ $item->title }}</a> </p>
                        <p class="center raleway txt-lgreen">by <a href="" class="txt-red">Putri Damayanti</a></p>
                        <p class="center txt-grey f12">
                            <i class="fa fa-heart pr5"></i>12
                            <i class="fa fa-comment pr5 ml10"></i>4
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection